
data "aws_availability_zones" "available" {
  filter {
    name   = "opt-in-status"
    values = ["opt-in-not-required"]
  }
}

locals {
  public_azs = { for idx, az in data.aws_availability_zones.available.names : az => idx
  if index(data.aws_availability_zones.available.names, az) < var.public_subnets_count }
  dmz_azs = { for idx, az in data.aws_availability_zones.available.names : az => idx
  if index(data.aws_availability_zones.available.names, az) < var.dmz_subnets_count }
}

resource "aws_vpc" "main-vpc" {
  cidr_block           = var.vpc_cidr_block
  enable_dns_hostnames = true
  instance_tenancy     = "default"
}

resource "aws_subnet" "public" {
  for_each                = local.public_azs
  map_public_ip_on_launch = true
  vpc_id                  = aws_vpc.main-vpc.id
  availability_zone       = each.key
  cidr_block              = cidrsubnet(var.vpc_cidr_block, var.mask_subnets, each.value)
}

resource "aws_subnet" "dmz" {
  for_each          = local.dmz_azs
  vpc_id            = aws_vpc.main-vpc.id
  availability_zone = each.key
  cidr_block        = cidrsubnet(var.vpc_cidr_block, var.mask_subnets, each.value + var.subnet_shift)
}

resource "aws_internet_gateway" "internet-gw" {
  vpc_id = aws_vpc.main-vpc.id
}

resource "aws_route_table" "public-rt" {
  for_each = local.public_azs
  vpc_id   = aws_vpc.main-vpc.id
}

resource "aws_route_table" "dmz-rt" {
  for_each = local.dmz_azs
  vpc_id   = aws_vpc.main-vpc.id
}

resource "aws_route" "public" {
  for_each               = local.public_azs
  route_table_id         = aws_route_table.public-rt[each.key].id
  gateway_id             = aws_internet_gateway.internet-gw.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "public" {
  depends_on = [
    aws_subnet.public,
    aws_route_table.public-rt,
  ]

  for_each       = local.public_azs
  subnet_id      = aws_subnet.public[each.key].id
  route_table_id = aws_route_table.public-rt[each.key].id
}

resource "aws_eip" "nat-gw-eip" {
  for_each = local.public_azs

  vpc = true

  lifecycle {
    create_before_destroy = true
  }
}

resource "aws_nat_gateway" "nat-gw-public" {
  for_each = local.public_azs

  allocation_id = aws_eip.nat-gw-eip[each.key].id
  subnet_id     = aws_subnet.public[each.key].id
}

resource "aws_route" "default" {
  depends_on = [aws_route_table.dmz-rt]
  for_each   = aws_route_table.dmz-rt

  route_table_id         = aws_route_table.dmz-rt[each.key].id
  nat_gateway_id         = try(aws_nat_gateway.nat-gw-public[each.key].id != null, false) ? aws_nat_gateway.nat-gw-public[each.key].id : aws_nat_gateway.nat-gw-public[keys(aws_nat_gateway.nat-gw-public)[0]].id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route_table_association" "dmz" {
  for_each = local.dmz_azs

  subnet_id      = aws_subnet.dmz[each.key].id
  route_table_id = aws_route_table.dmz-rt[each.key].id
}
