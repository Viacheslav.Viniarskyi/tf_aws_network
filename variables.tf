variable "dmz_subnets_count" {
  description = "Number of dmz subnets"
  type = number
}

variable "public_subnets_count" {
  description = "Number of public subnets, used for NAT gateways, one NAT gateway per subnet"
  type = number
}

variable "vpc_cidr_block" {
  description = "CIDR block for subnets calculation"
  type = string
}

variable "mask_subnets" {
  description = "Subnet mask number for subnets calculation"
  type = number
}

variable "subnet_shift" {
  type = number
}
